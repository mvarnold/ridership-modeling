import numpy as np
import pandas as pd
import pprint
import seaborn as sns
import matplotlib.pyplot as plt

class FormatPrinter(pprint.PrettyPrinter):

    def __init__(self, formats):
        super(FormatPrinter, self).__init__()
        self.formats = formats

    def format(self, obj, ctx, maxlvl, lvl):
        if type(obj) in self.formats:
            return self.formats[type(obj)] % obj, 1, 0
        return pprint.PrettyPrinter.format(self, obj, ctx, maxlvl, lvl)

populations = {'NYC': 23.7,
               'ALB': 1.1,
               'RUT': 0.015,
               'MID': 0.01,
               'BTN': 0.22,
               'MTR': 4.09,
               }
mileposts = {'NYC': 310,
             'ALB': 169,
             'RUT': 67,
             'MID': 34,
             'BTN': 0,
             'MTR': -(40+57),
             }

track_ave_speed = {}

routes = {'EAX' :
              [ ('MTR', 4.09, -(40+57)),
                ('BTN', 0.22, 0),
                ('MID', 0.01, 34),
                ('RUT', 0.015, 67),
                ('ALB', 1.1, 169),
                ('NYC', 23.7, 310),
              ],
          'EAX2' :
              [ #('MTR', 4.09, -(40+57)),
                ('BTN', 0.22, 0),
                ('MID', 0.01, 34),
                ('RUT', 0.015, 67),
                ('ALB', 1.1, 169),
                ('NYC', 23.7, 310),
              ],
         'LHV' :
             [ ("NYC", 23.7, 0),
               ('ALN', 0.85, 94),
               ('RDG', 0.42, 129),
               ('LBN', 0.14, 151),
               ('HAR', 0.56, 172),
              ],
          'KEY' : # should have 240k passengers for HAR, 270k for Lancaster, outputing 3.2x that
            [ ("NYC", 23.7, 0),
              ("TRN", 0.38, 58),
              ("PHL", 6.24, 91),
              ("LAN", 0.55, 159),
              ("HAR", 0.56, 195),
             ],
         'VTR' : 
             [ ('MTR', 4.09, -89),
               ('ESX', 0.22, 0),
               ('MPR', 0.016, 56),
               ('WRJ', 0.05, 118),
               ('BRA', 0.03, 181),
               ('GFD', 0.07, 205),
               ('NHT', 0.1, 224),
               ('SPR', 0.7, 245),
               #('HRT', 1.2, 271),
               #('NHV', 1.0, 308),
               #('BRP', 0.95, 330),
               ('NYC', 23.7, 379),
              ],
        'VTR2' :
             [ #('MTR', 4.09, -89),
               ('ESX', 0.22, 0),
               ('MPR', 0.016, 56),
               ('WRJ', 0.05, 118),
               ('BRA', 0.03, 181),
               ('GFD', 0.07, 205),
               ('NHT', 0.1, 224),
               ('SPR', 0.7, 245),
               #('HRT', 1.2, 271),
               #('NHV', 1.0, 308),
               #('BRP', 0.95, 330),
               ('NYC', 23.7, 379),
              ],
          'BLFX':
             [ ('MIA',2.7,0),
               ('FLD',1.9,31),
               ('WPB',1.4,69),
               ('ORL',2.6,235),
             ],
          'DME':
            [ ("BOS",4.1,0),
              ("EXE",0.3,51),
              ("WEL",0.2,84),
              ("POR",0.3,116),
              ('BRK',0.14,145),
             ],
            'PSL':
            [ ("SLA",0.065,0),
              ("GAU",0.14,24),
              ("LPS",0.06,51),
              ("SBR",0.09,119),
              ('VEC',0.1,145),
              ('OXN',0.2,156),
              ('CAM',0.07,164),
             ],
            'HFR':
            [ ('WIN',0.3,-231),
              ("TOR",6.3,0),
              ("OTW",1.5,249),
              ("MNT",4.09,399),
              ('QUB',0.8,561),
             ],
            'BLW':
            [ ('LA',13,0),
              ('LV',2.2,218),
             ],

        }


def gravity_model(pop_1, pop_2, time, alpha=0.5):
    """ Alon's gravity model for travel time with coefficient scaled for Empire Service

    For Alon's German HSR model alpha was 1.8. We're using 0.5 to approximate empire service which is essentially drive time compariable. 

    Ideally we'd implement a modeshare factor that depends on competing travel times,
    as the current model likely penalizes higher speed options too much. 
    
    Args:
        pop_1: population of city 1 in millions
        pop_2: population of city 2 in millions
        time: travel time between city pair in hours
        alpha: ridership co-efficient, set to 0.5 by default to approximate empire service

    Returns:
        demand_potential: An upperbound estimate of potential ridership demand,
            if frequent service is provided 

    """

    daily_demand = alpha * (pop_1 ** 0.8) * (pop_2 ** 0.8) / np.max([time ** 2, 5])

    return daily_demand

def segment_travel_time(milepost_1, milepost_2, ave_speed):
    """ Given a Vector of mile posts, return a vector of travel times between stations """
    return np.abs(milepost_2-milepost_1) / ave_speed

def get_travel_time(start, end, route, speeds):
    time_total = 0
    add = 0
    for i,station in enumerate(route):
        if station[0] == start:
            add = 1

        if station[0] == end:
            add = 0

        if add:
            time_total += segment_travel_time(route[i][2],route[i+1][2], speeds[i])

    #print(f"Total travel time between {start} and {end} is {time_total:.2f} hours")

    return time_total


def route_total(routes, name, speeds):
    """ sum ridership potential for all route segments in route
    
    Args:
        route: A list of stations containing a tuple of 
            (Station code, population in millions, milepost for route)

        speeds: a target average speed for the route given as a float or 
            a tuple of floats corresponding toeach route segment. 
    Returns:
        Total ridership in millions

    """
    route = routes[name]
    if type(speeds) == float or type(speeds) == int or type(speeds) == np.float64:
        speeds = [speeds for i in range(len(route))]

    index = 0 
    ridership = []
    segments_dict = {}

    for i, segment_start in enumerate(route):
        for j, segment_end in enumerate(route):
            if index < j and i < j:
                segments_dict[(segment_start[0], segment_end[0])] = {}

                segment_time = get_travel_time(segment_start[0], segment_end[0], route, speeds)
                segments_dict[(segment_start[0], segment_end[0])]['time'] = segment_time

                riders = gravity_model(segment_start[1], segment_end[1], segment_time)
                segments_dict[(segment_start[0], segment_end[0])]['ridership'] = riders
                ridership.append(riders)

    #print(f"route total {name} ridership is {np.sum(ridership):.3f}M with speeds {speeds[0]} mph")
    #FormatPrinter({float: "%.2f", int: "%06X"}).pprint(segments_dict)
    return segments_dict, np.sum(ridership)



def station_predictions(station, segments):
    ridership = 0
    for key,value in segments.items():
        if key[0] == station or key[1] == station:
            #print(key, f" ridership: {value['ridership']:.2f}M") 
            ridership += value['ridership']
    if ridership == 0:
        print(station)
        print(segments)
        raise KeyError
    print(f'Total {station} ridership: {ridership*10**6/365:.2f} / day')    
    #print(f'Total {station} ridership: {ridership:.2f}M / year')
    return ridership

def occupancy_by_station(station, segments):
    flow = {'on':0, 'off':0}
    for key,value in segments.items():
        if key[0] == station:
            # add rider
            flow['on'] += value['ridership'] *10**6/365

        if key[1] == station:
            flow['off'] -= value['ridership'] *10**6/365
    return flow

def all_station_occupancy(route, segments):
    load = {}
    for station_code in route:
        load[station_code[0]] = occupancy_by_station(station_code[0], segments)
    return load


def all_station_predictions(route, segments):
    results = {}
    for station_code in route: # (start station, end_station), {'ridership': float, 'time': float}
        results[station_code[0]] = station_predictions(station_code[0], segments)
    return results
        
def plot_station_ridership_by_speed(results):

    df = pd.DataFrame(results)
    df['station'] = df.index.values
    
    df['%_increase_to_60'] = ((df[60]/df[33]) - 1) * 100  
    df['%_increase_to_80'] = ((df[80]/df[33]) - 1) * 100

    df1 = pd.melt(df[[33, 60, 80, 'station']], id_vars='station')
    df1.rename(columns={"variable": "average speed", 'value':"Ridership"}, inplace=True)
    sns.catplot(x = 'station', y='Ridership',
               hue = 'average speed',data=df1, kind='bar')
    plt.title(route)
    plt.show()
    
    df2 = pd.melt(df[[ '%_increase_to_60', '%_increase_to_80', 'station']], id_vars='station')
    df2.rename(columns={"variable": "average speed", 'value':"Ridership increase (%)"}, inplace=True)
    sns.catplot(x = 'station', y='Ridership increase (%)',
               hue = 'average speed',data=df2, kind='bar')
    plt.title(route)
    plt.show()
    
def plot_loading_by_station(route, results, route_name, speed, plot=None, plot_index=None):
    stations = [code[0] for code in route]
    mileposts = [code[2] for code in route]
    ridership = []
    for i in results.values():
        ridership.append(np.sum(list(i.values())))
    ridership = np.cumsum(ridership)
    
    on_off_df = pd.DataFrame(results).T
    on_off_df['station'] = on_off_df.index 
    on_off_df = pd.melt(on_off_df, id_vars='station',  value_name='Passengers On/Off', var_name="Passengers")
    on_off_df['Passengers On/Off'] = np.abs(on_off_df['Passengers On/Off'].values)/2
    if plot == None:
        f, ax = plt.subplots(2,1)
    else:
        f, ax = plot
    sns.barplot(on_off_df, y="station", x="Passengers On/Off", hue="Passengers",
                ax=ax[plot_index])
    ax[plot_index].bar_label(ax[plot_index].containers[0], fmt='%.f',fontsize=6)
    ax[plot_index].bar_label(ax[plot_index].containers[1], fmt='%.f',fontsize=6)


titles = {'EAX': "Ethan Allen (Montreal Extention)",
          "EAX2": "Ethan Allen (Current Route)",
          "VTR": "Vermonter (Montreal Extension)",
          "VTR2": "Vermonter (Current Route)",
          "BLFX": "Brightline",
          'DME': "Downeaster",
          }

np.set_printoptions(precision=2)
print('------------------------------')
print()
for route in ["VTR", "EAX",'EAX2',"DME"]:#, 'KEY','LHV', 'BLFX']:
    results = {}
    plt.style.use('ggplot')
    f,ax = plt.subplots(3,1, sharey='row',sharex=True, figsize=(6,8))
    for i,speed in enumerate([41, 60, 80]):
        sixty, ridership = route_total(routes,route, speed)
        print(f"route total {route} ridership is {ridership:.3f}M with speeds {speed} mph")
        """#station_predictions('MTR', sixty)
        try:
            station_predictions('BTN', sixty)
        except KeyError:
            station_predictions('ESX', sixty)
            pass"""
        results[speed] = all_station_predictions(routes[route],sixty)
        print()
        net_ridership = all_station_occupancy(routes[route],sixty)
        plot_loading_by_station(routes[route],net_ridership, route, speed, (f,ax),i)
        ax[i].set_ylabel(f'{speed} MPH Average \n Station [per day]')
    plt.suptitle(titles[route])
    plt.tight_layout()
    plt.savefig(f"figures/{route}_station_ridership.png", dpi=300)
    plt.show()
    #plot_station_ridership_by_speed(results)
    print('------------------------------')
    print()


speeds = np.linspace(30,120, 500)
E = []
E2 = []
V = []
V2 = []
B = []
H = []
for speed in speeds:
    E.append(route_total(routes,'EAX',speed)[1])
    V.append(route_total(routes,'VTR',speed)[1])
    E2.append(route_total(routes,'EAX2',speed)[1])
    V2.append(route_total(routes,'VTR2',speed)[1])
    B.append(route_total(routes,'BLW',speed)[1])
    H.append(route_total(routes,'HFR',speed)[1])
    
plt.style.use('ggplot')

plt.figure(figsize=(8,7))
plt.plot(speeds,E2,label="Ethan Allen Express (Current)")
plt.plot(speeds,V2,label='Vermonter (Current)', color='green')
plt.plot(speeds,E,label="Ethan Allen Express (Montreal Extension)",color='#CA533B', linestyle='-.')
plt.plot(speeds,V,label='Vermonter (Montreal Extension)',color='green', linestyle='-.')
plt.plot(speeds,B,label='Brightline',color='orange')
plt.plot(speeds,H,label='High Frequency Rail',color='purple')
plt.axvline(36+5, alpha=.5,linestyle='--')
plt.text(29.8+5,2.5,'Current \nEAX speed: 36 mph',rotation=90)
plt.axvline(48, alpha=0.5, linestyle='--', color='green')
plt.text(44.8,2.5,'VTR speed: 48  mph',rotation=90)
plt.axvline(73, alpha=0.5, linestyle='--', color='orange')
plt.text(67.2,2.5,'Current \nBLFX speed: 73 mph',rotation=90)
plt.legend(bbox_to_anchor=(0.8,-0.11))


plt.ylabel('Annual Ridership Potential \n [millions]')
plt.xlabel('Average Speed [MPH]')
plt.tight_layout()
plt.savefig('figures/Brightline_compare.png',dpi=300)
plt.show()

